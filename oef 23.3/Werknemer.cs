﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oef_23._3
{
    internal class Werknemer
    {
        private string _naam;
        private string _voornaam;
        private double _verdiensten;

        public Werknemer() { }
        public Werknemer(string naam, string voornaam, double verdiensten)
        {
            Naam = naam;
            Voornaam = voornaam;
            Verdiensten = verdiensten;
        }

        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }
        public double Verdiensten
        {
            get { return _verdiensten; }
            set { _verdiensten = value; }
        }
        public string VolledigeWeergave
        {
            get { return Naam.PadLeft(10) + Voornaam.PadLeft(10) + Verdiensten.ToString().PadLeft(10) + "€"; }
        }
    }
}
