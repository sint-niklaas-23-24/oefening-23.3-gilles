﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace oef_23._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        Werknemer werknemer1;
        List<Werknemer> lijstWerknemers = new List<Werknemer>();
        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!(String.IsNullOrEmpty(txtAchternaam.Text) && String.IsNullOrEmpty(txtVoornaam.Text) && String.IsNullOrEmpty(txtVerdiensten.Text)))
                {
                    if (int.TryParse(txtVerdiensten.Text, out var verdiensten) == true)
                    {
                        werknemer1 = new Werknemer(txtAchternaam.Text, txtVoornaam.Text, verdiensten);
                        lijstWerknemers.Add(werknemer1);
                        lbVolledigeWeergave.Items.Add(werknemer1.VolledigeWeergave);
                    } else
                    {
                        throw new FormatException();
                    }
                } else
                {
                    throw new FormatException();
                }

            } catch (FormatException)
            {
                MessageBox.Show("U heeft of niet alle velden ingevuld, of Verdiensten is geen numerieke waarde", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
